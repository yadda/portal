#!/bin/bash

if [[ ! "$(kubectl -n yadda get secrets -o name)" =~ "google-secret" ]]; then
  kubectl -n yadda create secret generic google-secret \
	  --from-literal=user_id=$YADDA_ID \
   	  --from-literal=pass_key=$YADDA_KEY
fi

if [[ ! "$(kubectl -n yadda get secrets -o name)" =~ "captcha-secret" ]]; then
  kubectl -n yadda create secret generic captcha-secret \
	  --from-literal=captcha_public_key=$YADDA_CAPTCHA_PUBLIC_KEY \
   	  --from-literal=captcha_private_key=$YADDA_CAPTCHA_PRIVATE_KEY
fi

if [[ ! "$(kubectl -n yadda get secrets -o name)" =~ "email-secret" ]]; then
  kubectl -n yadda create secret generic email-secret \
	  --from-literal=username=$GOOGLE_SMTP_USER \
   	  --from-literal=password=$GOOGLE_SMTP_PASS
fi

kubectl -n yadda apply -f deployment.yaml

