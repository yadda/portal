#!/bin/bash

google-chrome --incognito --new-window https://127.0.0.1:5000
docker run --rm -e GOOGLE_CLIENT_ID=$YADDA_ID \
	        -e GOOGLE_CLIENT_KEY=$YADDA_KEY \
	        -e FLASK_DEBUG=1 \
	        -p 5000:5000 --name yadda sybex/portal:build
