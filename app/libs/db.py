from flask_login import UserMixin
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()
SQLALCHEMY_TRACK_MODIFICATIONS = False


class UserModel(UserMixin, db.Model):
    __tablename__ = 'users'

    id = db.Column(db.String(24), primary_key=True)
    name = db.Column(db.String(80), nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    profile_pic = db.Column(db.String(80), nullable=False)

    def __repr__(self):
        return f"<User {self.user}>"


class ProductModel(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    tag = db.Column(db.String(20), nullable=False)
    name = db.Column(db.String(40), nullable=False)
    desc = db.Column(db.String(80), nullable=True)
    price = db.Column(db.Float, nullable=False)
    spec1 = db.Column(db.Integer, nullable=False)
    spec2 = db.Column(db.Integer, nullable=False)
    spec3 = db.Column(db.Integer, nullable=False)
