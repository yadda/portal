from flask_mail import Mail, Message

mail = Mail()


def mail_contact(name, text, mail_to, reply_to):
    email = Message(body=f"From {name}: \n\n {text}",
                    subject="Contact from yadda portal",
                    recipients=[mail_to],
                    reply_to=reply_to)
    return email
