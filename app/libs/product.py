from libs.db import DBProduct


class Product():
    def __init__(self, id_, name, ):
        self.id = id_
        self.name = name
        self.database = DBProduct()

    @staticmethod
    def get(user_id):
        user = DBProduct.query.filter_by(username=user_id).first()

        if not user:
            return None

        product = DBProduct(
            id_=user[0], name=user[1], email=user[2], profile_pic=user[3]
        )
        return product

    @staticmethod
    def create(name, email, profile_pic):
        DBProduct(user=name, email=email, profile_pic=profile_pic)
