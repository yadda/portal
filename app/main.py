#!/usr/bin/env python3
# -*- coding: utf-8 -*-

""" main application """

import os
import logging

# internal libraries
from libs.mail import mail
from libs.db import db
from libs.db import UserModel, ProductModel

# Third-party libraries
from flask import Flask, render_template
from flask_login import LoginManager
from flask_compress import Compress
from flask_session import Session

# import blueprints
from auth import auth as auth_blueprint
from portal import portal as portal_blueprint

logging.basicConfig(level=logging.INFO)

# Flask app setup
app = Flask(__name__)
app.config['RECAPTCHA_PUBLIC_KEY'] = os.getenv("RECAPTCHA_PUBLIC_KEY", None)
app.config['RECAPTCHA_PRIVATE_KEY'] = os.getenv("RECAPTCHA_PRIVATE_KEY", None)
app.secret_key = os.getenv("SECRET_KEY", os.urandom(24))

# Global compression
app.config["COMPRESS_ALGORITHM"] = "gzip"
compress = Compress()
compress.init_app(app)

# E-Mail config
mail.init_app(app)
port = os.getenv("EMAIL_PORT", None)
if port:
    app.config["MAIL_SERVER"] = os.getenv("EMAIL_HOST", None)
    app.config["MAIL_PORT"] = int(port)
    app.config["MAIL_USERNAME"] = os.getenv("EMAIL_USER", None)
    app.config["MAIL_PASSWORD"] = os.getenv("EMAIL_PASS", None)
    app.config["MAIL_DEFAULT_SENDER"] = os.getenv("EMAIL_SENDER", None)

    if port == "465":
        app.config["MAIL_USE_SSL"] = True
    if port == "587":
        app.config["MAIL_USE_TLS"] = True

# Database
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:////data/portal.db"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
db.app = app
db.init_app(app)
db.create_all()

# Test data
debug = os.getenv("DEBUG", None)
if debug:
    product = ProductModel(name="caas-xs", desc="XS", tag="1",
                           spec1=0.5, spec2=1, spec3=4, price=3)
    db.session.add(product)
    product = ProductModel(name="caas-m", desc="M", tag="2",
                           spec1=2, spec2=4, spec3=16, price=7)
    db.session.add(product)
    product = ProductModel(name="caas-l", desc="L", tag="3",
                           spec1=4, spec2=8, spec3=32, price=13)
    db.session.add(product)
    db.session.commit()

# User management
login_manager = LoginManager()
login_manager.init_app(app)

# Session management
app.config["SESSION_TYPE"] = "sqlalchemy"
app.config["SESSION_SQLALCHEMY"] = db
session = Session(app)
session.app.session_interface.db.create_all()

# Register blueprints
app.register_blueprint(auth_blueprint)
app.register_blueprint(portal_blueprint)


# Flask-Login helper to retrieve a user from our db
@login_manager.user_loader
def load_user(user_id):
    return UserModel.query.get(user_id)


@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.jinja'), 404


if __name__ == "__main__":
    app.run(ssl_context="adhoc", host='0.0.0.0', threaded=True)
