#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
from libs.db import db, UserModel, ProductModel
from libs.mail import mail, mail_contact
from flask_wtf import FlaskForm, RecaptchaField
from wtforms import StringField
from flask import Blueprint, render_template, request, session, url_for, redirect
from flask_login import (
    current_user,
    login_required,
)

portal = Blueprint("portal", __name__)
PORTAL_ADMIN = os.getenv("PORTAL_ADMIN", None)
EMAIL_RECIPIENT = os.getenv("EMAIL_RECIPIENT", None)


class ContactForm(FlaskForm):
    name = StringField("Name")
    mail = StringField("E-Mail")
    text = StringField("Message")
    recaptcha = RecaptchaField()


class ProductForm(FlaskForm):
    name = StringField("Name")
    desc = StringField("Description")
    tag = StringField("Tag")
    spec1 = StringField("Specification 1")
    spec2 = StringField("Specification 2")
    spec3 = StringField("Specification 3")
    price = StringField("Price")


@portal.route("/")
def index():
    if current_user.is_authenticated:
        print("INFO: user is authenticated")
        return render_template("index.jinja",
                               authenticated=True,
                               email=current_user.email,
                               avatar=current_user.profile_pic)
    else:
        print("INFO: user is not authenticated")
        return render_template("index.jinja")


@portal.route("/health")
def health():
    """ used for checks from outside """
    try:
        # just test if db connection is working
        UserModel.query.filter_by(id=1).first()
    except Exception as err:
        print(f"ERROR: database is not provisionized => {err}")

    return "ok"


@portal.route("/products", methods=['POST', 'GET'])
def products():
    if request.method == "GET":
        products = ProductModel.query.all()
        return render_template("products.jinja", products=products)
    if request.method == "POST" and current_user.is_authenticated:
        product = ProductModel(name=request.form["name"],
                               desc=request.form["desc"],
                               tag=request.form["tag"],
                               spec1=request.form["spec1"],
                               spec2=request.form["spec2"],
                               spec3=request.form["spec3"],
                               price=request.form["price"])
        db.session.add(product)
        db.session.commit()
        return redirect(url_for("portal.admin"))


@portal.route("/cart", methods=['GET'])
@portal.route("/cart/<int:product_id>", methods=['POST'])
@portal.route("/cart/<int:product_id>/<string:action>", methods=['GET'])
def cart(product_id=0, action=None):
    if request.method == "GET":
        if action and action == "delete":
            for i in range(len(session["cart"])):
                if session["cart"][i]['id'] == product_id:
                    del session["cart"][i]
                    break
            return render_template("cart.jinja")

        if "cart" in session:
            cart = session.get("cart")
            print(f"INFO: cart content {cart}")
            return render_template("cart.jinja", cart=cart)
        return render_template("cart.jinja")

    if request.method == "POST":
        product = ProductModel.query.filter_by(id=product_id).first()
        pdict = {"id": product.id,
                 "name": product.name,
                 "price": product.price,
                 "desc": product.desc}
        print(f"INFO: adding to cart: {pdict}")

        if "cart" in session:
            session['cart'].append(pdict)
        else:
            session["cart"] = [pdict]

        return redirect(url_for("portal.products"))


@portal.route("/contact", methods=['POST', 'GET'])
def contact():
    if request.method == "GET":
        form = ContactForm()
        return render_template("contact.jinja", form=form)

    if request.method == "POST":
        status = False
        name = request.form["name"]
        reply_to = request.form["mail"]
        text = request.form["text"]

        if EMAIL_RECIPIENT:
            email = mail_contact(name, text, EMAIL_RECIPIENT, reply_to)
            try:
                status = mail.send(email)
            except Exception as err:
                print(f"ERROR: mail could not be send: {err}")

        return render_template("contact.jinja", name=name, status=status)


@portal.route('/profile')
@login_required
def profile():
    return render_template("profile.jinja")


@portal.route('/admin')
@portal.route('/admin/<string:page>')
@login_required
def admin(page=""):
    if current_user.email == PORTAL_ADMIN:
        if page:
            form = ProductForm()
            return render_template(f"admin-{page}.jinja", form=form)
        return render_template("admin.jinja")
    return redirect(url_for("portal.dashboard"))


@portal.route('/dashboard')
@login_required
def dashboard():
    if current_user.email == PORTAL_ADMIN:
        return redirect(url_for("portal.admin"))
    return render_template("dashboard.jinja")
