FROM alpine:3.14
LABEL maintainer="yadda"

RUN apk --no-cache add py3-pip py3-cryptography curl sqlite py3-brotli && \
    pip3 install --no-cache-dir --upgrade pip && \
    pip3 install --no-cache-dir flask \
    				flask-wtf \
    				flask-mail \
    				flask-login \
    				flask-session \
				flask-compress \
				flask-sqlalchemy \
				"sqlalchemy<1.4" \
				wtforms \
				requests \
				oauthlib \
				pyOpenSSL \
				flake8 pylint

EXPOSE 5000
COPY app/ /app

RUN adduser -D web && \
    chown -R web:root /app && \
    chmod -R 2775 /app && \
    mkdir /data && chown web /data && \
    \
    python3 -m compileall /app/*.py && \
    python3 -m compileall /app/libs/*.py && \
    flake8 --max-line-length=120 /app/
    # pylint --max-line-length=120 /app/

WORKDIR /app
USER web
ENV PYTHONUNBUFFERED=TRUE

CMD python3 /app/main.py
