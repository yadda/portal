#!/bin/bash

set -e

ORG="sybex"
APP="portal"
VER="build"
PUSH="$1"

cleanup() {
    echo -e "\n\nLogs:"
    docker logs $APP

    echo -e "\nCleanup:"
    docker stop $APP
    docker container prune --force
    docker image prune --force
}

clear
trap cleanup EXIT
trap cleanup SIGTERM

# build
docker build -t $ORG/$APP:$VER .
docker run -e GOOGLE_CLIENT_ID=$YADDA_ID \
	   -e GOOGLE_CLIENT_KEY=$YADDA_KEY \
	   -d -p 5000:5000 --name $APP $ORG/$APP:$VER

echo -e "\nImage:"
docker images | grep $APP
sleep 2

echo -e "\nTest:"
curl -k -H "Accept-Encoding: gzip" -I https://127.0.0.1:5000/health

# push
if [ "$PUSH" == "push" ]; then
  docker tag $ORG/$APP:$VER $ORG/$APP:latest
  docker push $ORG/$APP:latest
  kubectl --context yadda -n yadda delete po -l app=yadda
fi
